$( document ).ready(function() {
    $("#id-draft").on('click', function (e) {
        e.preventDefault()
        //alert(urlNewDraft)

        var msg = CKEDITOR.instances['courrier_message'].getData();
        $("#msg-id-draft").val(msg);
        var form = new FormData($('form')[0]);
        if($(".recep").val() == ''){
            window.location.replace(urlMailBox);
        }else{
            $.ajax({
                url: urlNewDraft,
                method: "POST",
                dataType: "json",
                cache: false,
                processData: false,
                contentType: false,
                data: form,
                success: function(data) {
                    window.location.replace(urlDraft);
                },
                error: function(error) {
                    console.log('erreur ajax');
                }
            })
        }
    });
});