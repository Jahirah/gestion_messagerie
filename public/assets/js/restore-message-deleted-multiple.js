$(document).ready(function () {
    $("#form-btn-mailbox").attr("action", path_trash_multiple);

    $(".btn-restore").on("click", function (e) {
        e.preventDefault();
        $("#form-btn-mailbox").attr("action", path_restore_trash_multiple);

        let value_checked_id = [];

        $(".checkbox_multiples:checked").each(function () {
            value_checked_id.push($(this).val());
        });

        $.ajax({
            type: "POST",
            url: $("#form-btn-mailbox").attr("action"),
            data: {id_message_to_archived: value_checked_id},
            dataType: "json",
            success: function (response) {
                window.location.reload();
            },
            error: function (error) {
                $(".error").before("<div class='alert alert-danger'>Veuillez d'abord cocher</div>");
                console.log(error);
            }
        });
    });
});