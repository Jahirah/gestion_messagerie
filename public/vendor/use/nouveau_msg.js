$(document).ready(function () {
    $("#send_msg").on('click', function (e) {
        //$("#img").show();
        //e.preventDefault();
        var msg = CKEDITOR.instances['courrier_message'].getData();
        $("#msg-id").val(msg);
        console.log(msg);
        $.ajax({
            url: urlSend,
            method: "POST",
            dataType: "json",
            cache: false,
            processData: false,
            contentType: false,
            data: new FormData($('form')[0]),
            success: function(data) {
                $("#img").hide();
              window.location.replace(urlOutboxredirect);
            },
            error: function(error) {
                console.log('erreur ajax');
            }
        })
    });
})