$(document).ready(function () {
    // setTimeout(function(){ $("#alert-success").fadeOut(); }, 5000);
    
    $("#form-btn-mailbox").attr("action", path_trash_multiple);

    $("#checkAll").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
    });

    $(".btn-archive").on("click", function (e) {
        e.preventDefault();
        $("#form-btn-mailbox").attr("action", path_archive_multiple);

        let value_checked_id = [];

        $(".checkbox_multiples:checked").each(function () {
            value_checked_id.push($(this).val());
        });

        $.ajax({
            type: "POST",
            url: $("#form-btn-mailbox").attr("action"),
            data: {id_message_to_archived: value_checked_id, route: path_box},
            dataType: "json",
            success: function (response) {
                window.location.reload();
            },
            error: function (error) {
                console.log(error);
                $(".error").before("<div class='alert alert-danger'>Veuillez d'abord cocher</div>");
            }
        });
    });

    $(".btn-remove").on("click", function (e) {
        e.preventDefault();
        $("#form-btn-mailbox").attr("action", path_trash_multiple);

        let value_checked_id = [];

        $(".checkbox_multiples:checked").each(function () {
            value_checked_id.push($(this).val());
        });

        $.ajax({
            type: "POST",
            url: $("#form-btn-mailbox").attr("action"),
            data: {id_message_to_archived: value_checked_id, route: path_box},
            dataType: "json",
            success: function (response) {
                window.location.reload();
            },
            error: function (error) {
                $(".error").before("<div class='alert alert-danger'>Veuillez d'abord cocher</div>");
                console.log(error);
            }
        });
    });
});