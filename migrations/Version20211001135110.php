<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211001135110 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE courrier (id INT AUTO_INCREMENT NOT NULL, sender_id INT NOT NULL, object VARCHAR(70) DEFAULT NULL, message LONGTEXT DEFAULT NULL, file VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', is_read TINYINT(1) DEFAULT NULL, is_deleted TINYINT(1) DEFAULT NULL, is_archived TINYINT(1) DEFAULT NULL, is_shared TINYINT(1) DEFAULT NULL, is_draft TINYINT(1) DEFAULT NULL, INDEX IDX_BEF47CAAF624B39D (sender_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE courrier_user (courrier_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_343667B8BF41DC7 (courrier_id), INDEX IDX_343667BA76ED395 (user_id), PRIMARY KEY(courrier_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `user` (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, photo VARCHAR(255) DEFAULT NULL, phone_number VARCHAR(15) DEFAULT NULL, username VARCHAR(60) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE courrier ADD CONSTRAINT FK_BEF47CAAF624B39D FOREIGN KEY (sender_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE courrier_user ADD CONSTRAINT FK_343667B8BF41DC7 FOREIGN KEY (courrier_id) REFERENCES courrier (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE courrier_user ADD CONSTRAINT FK_343667BA76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE courrier_user DROP FOREIGN KEY FK_343667B8BF41DC7');
        $this->addSql('ALTER TABLE courrier DROP FOREIGN KEY FK_BEF47CAAF624B39D');
        $this->addSql('ALTER TABLE courrier_user DROP FOREIGN KEY FK_343667BA76ED395');
        $this->addSql('DROP TABLE courrier');
        $this->addSql('DROP TABLE courrier_user');
        $this->addSql('DROP TABLE `user`');
    }
}
