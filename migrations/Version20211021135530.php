<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211021135530 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE courrier ADD courrier_cc_id INT DEFAULT NULL, ADD courrier_cci_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE courrier ADD CONSTRAINT FK_BEF47CAA607A3F7F FOREIGN KEY (courrier_cc_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE courrier ADD CONSTRAINT FK_BEF47CAA180F1C4C FOREIGN KEY (courrier_cci_id) REFERENCES `user` (id)');
        $this->addSql('CREATE INDEX IDX_BEF47CAA607A3F7F ON courrier (courrier_cc_id)');
        $this->addSql('CREATE INDEX IDX_BEF47CAA180F1C4C ON courrier (courrier_cci_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE courrier DROP FOREIGN KEY FK_BEF47CAA607A3F7F');
        $this->addSql('ALTER TABLE courrier DROP FOREIGN KEY FK_BEF47CAA180F1C4C');
        $this->addSql('DROP INDEX IDX_BEF47CAA607A3F7F ON courrier');
        $this->addSql('DROP INDEX IDX_BEF47CAA180F1C4C ON courrier');
        $this->addSql('ALTER TABLE courrier DROP courrier_cc_id, DROP courrier_cci_id');
    }
}
