<?php

namespace App\Repository;

use App\Entity\Courrier;
use App\Entity\SortingMessage;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Courrier|null find($id, $lockMode = null, $lockVersion = null)
 * @method Courrier|null findOneBy(array $criteria, array $orderBy = null)
 * @method Courrier[]    findAll()
 * @method Courrier[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CourrierRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Courrier::class);
    }

    // /**
    //  * @return Courrier[] Returns an array of Courrier objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Courrier
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * Recupère les messages reçu par l'utilisateur
     * @param $email_sender
     * @return int|mixed[]|string
     */
    public function findAllMessageReceivedBy($recipient, SortingMessage $sorting_message)
    {
        $query = $this->createQueryBuilder('message');
        $query
            ->orderBy('message.createdAt', 'DESC')
            ->andWhere('message.isDeletedRecipient = 0')
            ->andWhere('message.isArchivedRecipient = 0')
            ->andWhere('message.isDraft = 0')
            ->leftJoin('message.recipient', 'recipient')
            ->andWhere('recipient.email = :email_recipient')
            ->setParameter('email_recipient', $recipient)
        ;

        if (!empty($sorting_message->getEmailFrom()) && !empty($sorting_message->getEmailTo())){

            $query
                ->leftJoin('message.sender', 'sender')
                ->andWhere('recipient.email = :recipient_email')
                ->andWhere('sender.email = :sender_email')
                ->setParameter('recipient_email', $sorting_message->getEmailTo())
                ->setParameter('sender_email',$sorting_message->getEmailFrom());
        }

        if (!empty($sorting_message->getEmailTo()) && !empty($sorting_message->getObject())){
            $query
                ->andWhere('recipient.email = :recipient_email')
                ->andWhere('message.object = :object')
                ->setParameter('recipient_email', $sorting_message->getEmailTo())
                ->setParameter('object',$sorting_message->getObject());
        }

        if (!empty($sorting_message->getEmailTo()) && !empty($sorting_message->getContentOf())){
            $query
                ->andWhere('recipient.email = :recipient_email')
                ->andWhere('message.message LIKE :content_message')
                ->setParameter('recipient_email', $sorting_message->getEmailTo())
                ->setParameter('content_message','%'.$sorting_message->getContentOf().'%');
        }

        return $query->getQuery()->getResult();
    }

    /**
     * compteur message non lu
     * @param $email_sender
     * @return int|mixed[]|string
     */
    public function countMessageReceivedNotReaded($email_sender)
    {
        return $this->createQueryBuilder('message')
            ->addSelect('COUNT( recipient.username) AS nbre_isNotRead')
            ->leftJoin('message.recipient', 'recipient')
            ->leftJoin('message.sender', 'sender')
            ->andWhere('recipient.email = :email_sender')
            ->andWhere('message.isRead = 0')
            ->setParameter('email_sender', $email_sender)
            ->getQuery()
            ->getResult();
    }

    /**
     * boite d'envoie
     * @param $sender
     * @return int|mixed|string
     */
    public function OutBoxRepository($sender)
    {
        return $this->createQueryBuilder('message')
            ->orderBy('message.createdAt', 'DESC')
            ->andWhere('message.sender = :val')
            ->andWhere('message.isDeletedSender = 0')
            ->andWhere('message.isDraft = 0')
            ->andWhere('message.isArchivedSender = 0')
            ->setParameter('val', $sender)
            ->getQuery()
            ->getResult();
    }

    /**
     * corbeille
     * @param $email_received
     * @return int|mixed[]|string
     */
    public function findAllMessageTrashBy($email_received)
    {
        return $this->createQueryBuilder('message')
            ->orderBy('message.createdAt', 'DESC')
            ->leftJoin('message.recipient', 'recipient')
            ->andWhere('recipient.email = :email_received')
            ->andWhere('message.isDeletedRecipient = 1')
            ->andWhere('message.isDeletedTrashRecipient = 0')
            ->andWhere('message.isDeletedTrashSender = 0')
            ->setParameter('email_received', $email_received)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $sender
     * @return int|mixed[]|string
     */
    public function OutBoxTrashBy($sender)
    {
        return $this->createQueryBuilder('message')
            ->orderBy('message.createdAt', 'DESC')
            ->leftJoin('message.recipient', 'recipient')
            ->leftJoin('message.sender', 'sender')
            ->orderBy('message.createdAt', 'ASC')
            ->andWhere('message.sender = :val')
            ->andWhere('message.isDeletedSender = 1')
            ->andWhere('message.isDeletedTrashSender = 0')
            ->setParameter('val', $sender)
            ->getQuery()
            ->getResult();
    }

    /**
     * archive reception
     * @param $email_received
     * @return int|mixed[]|string
     */
    public function findAllMessageArchivedBy($email_received)
    {
        return $this->createQueryBuilder('message')
            ->orderBy('message.createdAt', 'DESC')
            ->leftJoin('message.recipient', 'recipient')
            ->leftJoin('message.sender', 'sender')
            ->andWhere('recipient.email = :email_received')
            ->andWhere('message.isArchivedRecipient = 1')
            ->andWhere('message.isDeletedRecipient = 0')
            ->setParameter('email_received', $email_received)
            ->getQuery()
            ->getResult();
    }

    /**
     * archive boite d'envoie
     * @param $sender
     * @return int|mixed[]|string
     */
    public function OutBoxArchive($sender)
    {
        return $this->createQueryBuilder('message')
            ->orderBy('message.createdAt', 'DESC')
            ->leftJoin('message.recipient', 'recipient')
            ->leftJoin('message.sender', 'sender')
            ->orderBy('message.createdAt', 'ASC')
            ->andWhere('message.sender = :val')
            ->andWhere('message.isArchivedSender = 1')
            ->andWhere('message.isDeletedSender = 0')
            ->setParameter('val', $sender)
            ->getQuery()
            ->getResult();
    }

    /**
     * draft
     * @param $email_received
     * @return int|mixed[]|string
     */
    public function findAllMessageADraftetBy($email_received)
    {
        return $this->createQueryBuilder('message')
            ->addSelect('recipient.email AS recipient_email', 'sender.email AS sender_email', 'sender.username AS sender_username')
            ->leftJoin('message.recipient', 'recipient')
            ->leftJoin('message.sender', 'sender')
            ->andWhere('recipient.email = :email_received')
            ->andWhere('message.isArchived = 1')
            ->setParameter('email_received', $email_received)
            ->getQuery()
            ->getScalarResult();
    }

    /**
     * brouillons
     * @param $sender
     * @return int|mixed|string
     */
    public function DraftRepository($sender)
    {
        return $this->createQueryBuilder('message')
            ->orderBy('message.createdAt', 'DESC')
            ->andWhere('message.sender = :val')
            ->andWhere('message.isDraft = true')
            ->andWhere('message.isDeletedSender = 0')
            ->setParameter('val', $sender)
            ->getQuery()
            ->getResult();
    }

    public function findLastMessageReceived($last_message)
    {
        return $this->createQueryBuilder('email')
            ->addSelect('recipient.email AS recipient_email', 'sender.email AS sender_email', 'sender.username AS sender_username')
            ->leftJoin('email.recipient', 'recipient')
            ->leftJoin('email.sender', 'sender')
            ->andWhere('recipient.email = :email_received')
            ->orderBy('email.createdAt', 'DESC')
            ->andWhere('email.isDeletedRecipient = 0')
            ->andWhere('email.isArchivedRecipient = 0')
            ->setParameter('email_received', $last_message)
            ->setMaxResults(3)
            ->getQuery()
            ->getScalarResult();
    }

    public function getCourrierReceivedBy(User $recipient)
    {
        $qb = $this->createQueryBuilder('message');

        $qb->andWhere(':recipient MEMBER OF message.recipient')
            ->setParameters(['recipient' => $recipient]);

        return $qb->getQuery()->getResult();
    }
}
