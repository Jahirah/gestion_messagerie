<?php

namespace App\Form;

use App\Entity\SortingMessage;
use Symfony\Component\Form\AbstractType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class SortingMessageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('emailFrom', EmailType::class,[
                'label' => "De",
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('emailTo', EmailType::class,[
                'label' => "A",
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('object', TextType::class,[
                'label' => "Objet",
                'attr' => [
                    'class' => 'form-control'
                ],
                'required' => false
            ])
            ->add('contentOf', TextType::class,[
                'label' => "Contient de",
                'attr' => [
                    'class' => 'form-control'
                ],
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SortingMessage::class,
            'method' => 'GET',
            'csrf_protection' => false
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
