<?php

namespace App\Form;

use App\Entity\Courrier;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PartagerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('recipient', EntityType::class, [
                "label" => ' ',
                "class" => User::class,
                "choice_label" => "email",
                "attr" => array (
                    "class" => "form-control select2",
                    "placeholder" => "À"
                ),
                "multiple" => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(['data_class' => Courrier::class]);
    }
}
