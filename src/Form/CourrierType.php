<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Courrier;
use Symfony\Component\Form\AbstractType;
//use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class CourrierType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder

            ->add('recipient', EntityType::class, [
                "label" => ' ',
                "class" => User::class,
                "choice_label" => "email",
                "attr" => array (
                    "class" => "form-control select2 recep recipient",
                ),
                "by_reference" => false,
                "multiple" => true,
                "required" => true
            ])
            ->add('messageCcTo',EntityType::class,[
                "label" => ' ',
                'multiple' => true,
                'class' => User::class,
                'choice_label' => 'email',
                'required' => false,
                'mapped' => false,
                "attr" => array (
                    "class" => "form-control select2 message-cc",
                )
            ])
            ->add('messageCciTo',EntityType::class,[
                "label" => ' ',
                'multiple' => true,
                'class' => User::class,
                'choice_label' => 'email',
                'required' => false,
                'mapped' => false,
                "attr" => array (
                    "class" => "form-control select2 message-cci",
                )
            ])
            ->add('object', TextType::class, [
                "label" => ' ',
                "attr" => array (
                    "class" => "form-control",
                    'placeholder' => "Objet",
                    'maxlength' => 50
                ),
                "required" => false
            ])
            ->add('message', TextareaType::class, [
                "label" => ' ',
                "attr" => array (
                    'class' => "form-control",
                    'placeholder' => "Message"
                ),
                "required" => false
            ])
            ->add('file', FileType::class, [
                "label" => ' ',
                "mapped" => false,
                "multiple" => true,
                "attr" => array (
                   "class" => "btn btn-default btn-file",
                   "i class" => "fa fa-paperclip",
                   "label" => "Attachment"
                ),
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Courrier::class,
        ]);
    }
}
