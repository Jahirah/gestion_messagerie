<?php

namespace App\Entity;

class SortingMessage
{
    private $emailFrom;

    private $emailTo;

    private $object;

    private $contentOf;

    public function getEmailFrom(): ?string
    {
        return $this->emailFrom;
    }

    public function setEmailFrom(string $emailFrom): self
    {
        $this->emailFrom = $emailFrom;

        return $this;
    }

    public function getEmailTo()
    {
        return $this->emailTo;
    }

    public function setEmailTo($emailTo): void
    {
        $this->emailTo = $emailTo;
    }

    public function getObject(): ?string
    {
        return $this->object;
    }

    public function setObject(string $object): self
    {
        $this->object = $object;

        return $this;
    }

    public function getContentOf(): ?string
    {
        return $this->contentOf;
    }

    public function setContentOf(string $contentOf): self
    {
        $this->contentOf = $contentOf;

        return $this;
    }
}
