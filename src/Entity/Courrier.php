<?php

namespace App\Entity;

use App\Repository\CourrierRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=CourrierRepository::class)
 */
class Courrier
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="courriers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $sender;

    /**
     * @ORM\ManyToMany(targetEntity=User::class)
     * @Assert\NotBlank()
     */
    private $recipient;

    /**
     * @ORM\Column(type="string", length=70, nullable=true)
     */
    private $object;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $message;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $file;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isRead = 0;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDeleted = 0;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isArchived = 0;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isShared = 0;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDraft = 0;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDeletedSender = 0;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDeletedRecipient = 0;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isArchivedSender = 0;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isArchivedRecipient = 0;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDeletedTrashSender = 0;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDeletedTrashRecipient = 0;

    /**
     * @ORM\OneToMany(targetEntity=Files::class, mappedBy="messages", orphanRemoval=true,cascade={"persist"})
     */
    private $files;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $messageCc = [];

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $messageCci = [];

    public function __construct()
    {
        $this->recipient = new ArrayCollection();
        $this->createdAt = new \DateTimeImmutable();
        $this->files = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSender(): ?User
    {
        return $this->sender;
    }

    public function setSender(?User $sender): self
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getRecipient(): Collection
    {
        return $this->recipient;
    }

    public function addRecipient(User $recipient): self
    {
        if (!$this->recipient->contains($recipient)) {
            $this->recipient[] = $recipient;
        }

        return $this;
    }

    public function removeRecipient(User $recipient): self
    {
        $this->recipient->removeElement($recipient);

        return $this;
    }

    public function getObject(): ?string
    {
        return $this->object;
    }

    public function setObject(?string $object): self
    {
        $this->object = $object;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(?string $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getIsRead(): ?bool
    {
        return $this->isRead;
    }

    public function setIsRead(?bool $isRead): self
    {
        $this->isRead = $isRead;

        return $this;
    }

    public function getIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(?bool $isDeleted): self
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    public function getIsArchived(): ?bool
    {
        return $this->isArchived;
    }

    public function setIsArchived(?bool $isArchived): self
    {
        $this->isArchived = $isArchived;

        return $this;
    }

    public function getIsShared(): ?bool
    {
        return $this->isShared;
    }

    public function setIsShared(?bool $isShared): self
    {
        $this->isShared = $isShared;

        return $this;
    }

    public function getIsDraft(): ?bool
    {
        return $this->isDraft;
    }

    public function setIsDraft(?bool $isDraft): self
    {
        $this->isDraft = $isDraft;

        return $this;
    }

    public function getIsDeletedSender(): ?bool
    {
        return $this->isDeletedSender;
    }

    public function setIsDeletedSender(?bool $isDeletedSender): self
    {
        $this->isDeletedSender = $isDeletedSender;

        return $this;
    }

    public function getIsDeletedRecipient(): ?bool
    {
        return $this->isDeletedRecipient;
    }

    public function setIsDeletedRecipient(?bool $isDeletedRecipient): self
    {
        $this->isDeletedRecipient = $isDeletedRecipient;

        return $this;
    }

    public function getIsArchivedSender(): ?bool
    {
        return $this->isArchivedSender;
    }

    public function setIsArchivedSender(?bool $isArchivedSender): self
    {
        $this->isArchivedSender = $isArchivedSender;

        return $this;
    }

    public function getIsArchivedRecipient(): ?bool
    {
        return $this->isArchivedRecipient;
    }

    public function setIsArchivedRecipient(?bool $isArchivedRecipient): self
    {
        $this->isArchivedRecipient = $isArchivedRecipient;

        return $this;
    }

    public function getIsDeletedTrashSender(): ?bool
    {
        return $this->isDeletedTrashSender;
    }

    public function setIsDeletedTrashSender(?bool $isDeletedTrashSender): self
    {
        $this->isDeletedTrashSender = $isDeletedTrashSender;

        return $this;
    }

    public function getIsDeletedTrashRecipient(): ?bool
    {
        return $this->isDeletedTrashRecipient;
    }

    public function setIsDeletedTrashRecipient(?bool $isDeletedTrashRecipient): self
    {
        $this->isDeletedTrashRecipient = $isDeletedTrashRecipient;

        return $this;
    }

    /**
     * @return Collection|Files[]
     */
    public function getFiles(): Collection
    {
        return $this->files;
    }

    public function addFile(Files $file): self
    {
        if (!$this->files->contains($file)) {
            $this->files[] = $file;
            $file->setMessages($this);
        }

        return $this;
    }

    public function removeFile(Files $file): self
    {
        if ($this->files->removeElement($file)) {
            // set the owning side to null (unless already changed)
            if ($file->getMessages() === $this) {
                $file->setMessages(null);
            }
        }

        return $this;
    }

    public function getMessageCc(): ?array
    {
        return $this->messageCc;
    }

    public function setMessageCc(?array $messageCc): self
    {
        $this->messageCc = $messageCc;

        return $this;
    }

    public function getMessageCci(): ?array
    {
        return $this->messageCci;
    }

    public function setMessageCci(?array $messageCci): self
    {
        $this->messageCci = $messageCci;

        return $this;
    }
}
