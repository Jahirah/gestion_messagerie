<?php

namespace App\Controller;

use App\Entity\Courrier;
use App\Entity\SortingMessage;
use App\Form\SortingMessageType;
use App\Repository\CourrierRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class MenuController extends AbstractController
{
    /**
     * @Route("/menu", name="menu_messagerie")
     */
    public function indexMenu(Request $request,CourrierRepository $messageRepository): Response{
        $recipient = $this->getUser();

        if (empty($recipient)){
            return $this->redirectToRoute('app_login');
        }

        $sorting_message = new SortingMessage();
        $search_form = $this->createForm(SortingMessageType::class, $sorting_message);
        $search_form->handleRequest($request);
        $email_received = $recipient->getEmail();
        // message non reçu
        $email_receiveds = $messageRepository->findAllMessageReceivedBy($email_received, $sorting_message);
        // notification
        $last_messages = $messageRepository->findLastMessageReceived($email_received);
        // compte message reçu non lu
        $numberMsgNotRead = $messageRepository->countMessageReceivedNotReaded($email_received)[0]['nbre_isNotRead'];
		
        return $this->render('menu/menu.html.twig', [
            'email_receiveds' => $email_receiveds,
            'numberMsgNotRead' => $numberMsgNotRead,
            'last_messages' => $last_messages,
            'search_form' => $search_form->createView(),
        ]);
    }
}
