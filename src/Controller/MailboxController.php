<?php

namespace App\Controller;

use App\Entity\Courrier;
use App\Form\CourrierType;
use App\Entity\SortingMessage;
use App\Form\SortingMessageType;
use App\Repository\CourrierRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MailboxController extends AbstractController
{
    private $messageRepository;

    // Boîte de réception

    /**
     * MailboxController constructor.
     * @param CourrierRepository $messageRepository
     */
    public function __construct(CourrierRepository $messageRepository)
    {
        $this->messageRepository = $messageRepository;
    }

    /**
     * @Route("/boite-de-reception", name="app_mailbox")
     */
    public function index(Request $request): Response
    {
        $recipient = $this->getUser();

        if (empty($recipient)){
            return $this->redirectToRoute('app_login');
        }

        $sorting_message = new SortingMessage();
        $search_form = $this->createForm(SortingMessageType::class, $sorting_message);
        $search_form->handleRequest($request);
        $form = $this->createForm(CourrierType::class);

        $email_received = $recipient->getEmail();

        // message non reçu
        $email_receiveds = $this->messageRepository->findAllMessageReceivedBy($email_received, $sorting_message);

        // notification
        $last_messages = $this->messageRepository->findLastMessageReceived($email_received);

        // compte message reçu non lu
        $numberMsgNotRead = $this->messageRepository->countMessageReceivedNotReaded($email_received)[0]['nbre_isNotRead'];

        return $this->render('mail/reception.html.twig', [
            'email_receiveds' => $email_receiveds,
            'numberMsgNotRead' => $numberMsgNotRead,
            'last_messages' => $last_messages,
            'search_form' => $search_form->createView(),
            'form'=>$form->createView()
        ]);
    }
}
