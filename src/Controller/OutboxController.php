<?php

namespace App\Controller;

use App\Form\CourrierType;
use App\Repository\CourrierRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class OutboxController extends AbstractController
{
    // Boite d'envoie
    /**
     * @Route("/outbox", name="app_outbox")
     */
    public function index(CourrierRepository $courrierRepository): Response
    {
        $sender = $this->getUser();
        $outBox = $courrierRepository->OutBoxRepository($sender);
        $email_received = $sender->getEmail();
        $last_message_received = $courrierRepository->findLastMessageReceived($email_received);

        //modal pour nouveau message
        $form = $this->createForm(CourrierType::class)->createView();
        return $this->render('mail/envoie.html.twig', [
            'envoyes' => $outBox ? $outBox : null,
            'last_messages' => $last_message_received,
            'form' => $form
        ]);
    }
}
