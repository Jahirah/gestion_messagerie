<?php

namespace App\Controller;

use App\Entity\Courrier;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MarkAsReadController extends AbstractController
{
    /**
     * @Route("mark_as_read/{id}", name="mark_as_read", methods={"GET"})
     */
    public function markAsRead(Courrier $courrier): Response
    {
        if($courrier->getIsRead() == false){
            $courrier->setIsRead(true);
        }else{
            $courrier->setIsRead(false);
        }
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($courrier);
        $entityManager->flush();

        return $this->redirectToRoute('app_mailbox');
    }
}
