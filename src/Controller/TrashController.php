<?php

namespace App\Controller;

use App\Entity\Courrier;
use App\Form\CourrierType;
use App\Repository\CourrierRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TrashController extends AbstractController
{
    private $courrierRepository;

    /**
     * TrashController constructor.
     * @param $courrierRepository
     */
    public function __construct(CourrierRepository $courrierRepository)
    {
        $this->courrierRepository = $courrierRepository;
    }

    /**
     * Corbeille
     * @Route("/trash", name="app_trash")
     */
    public function index(CourrierRepository $courrierRepository): Response
    {
        $recipient = $this->getUser();
        $email_received = $recipient->getEmail();
        $last_message_received = $courrierRepository->findLastMessageReceived($email_received);
        $allMessageDeleted = array_merge($courrierRepository->findAllMessageTrashBy($email_received), $courrierRepository->OutBoxTrashBy($this->getUser()));
        
        //modal pour nouveau message
        $form = $this->createForm(CourrierType::class)->createView();

        return $this->render('mail/corbeille.html.twig', [
            'trashes' => $allMessageDeleted,
            'last_messages' => $last_message_received,
            'form' => $form
        ]);
    }

    /**
     * @Route("/{id}/{route}/trash", name="trash", methods={"GET"})
     */
    public function trash(Courrier $courrier, $route,Request $request): Response
    {
        $mailUser = $this->getUser()->getEmail();
        if ($route == "app_mailbox") {
            $courrier->setIsDeletedRecipient(true);
        }

        if ($route == "read") {
            $courrier->setIsDeletedRecipient(true);
        }

        if ($route == "app_outbox" or ($route == "app_archive" and $courrier->getSender()->getEmail() == $mailUser) ) {
            $courrier->setIsDeletedSender(true);
        }else{
            $courrier->setIsDeletedRecipient(true);
        }


        $entityManager = $this->getDoctrine()->getManager();
        if($route == "app_draft"){
            $entityManager->remove($courrier);
        }else{
            $entityManager->persist($courrier);
        }
        $entityManager->flush();


        return $route == "app_draft"  ? $this->redirectToRoute('app_draft') : $this->redirectToRoute('app_trash');
    }

    /**
     * @Route("/trash-multiple", name="trash_multiple")
     */
    public function trashMultiple(Request $request): Response
    {
        $data = $request->request->all();
        if (!$this->getUser()) {
            return $this->redirectToRoute("app_login");
        }

        $user = $this->getUser();

        $id_messages_to_archived = $data["id_message_to_archived"];
        $route = $data["route"];

        foreach ($id_messages_to_archived as $id_message_to_archived)
        {
            $message = $this->courrierRepository->find($id_message_to_archived);
            if ($route == "app_mailbox" || $user->getEmail() != $message->getSender()->getEmail() ) {
                $message->setIsDeletedRecipient(true);
            }

            if ($route == "app_outbox" || $user->getEmail() == $message->getSender()->getEmail()) {
                $message->setIsDeletedSender(true);
            }
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($message);
        $entityManager->flush();

        $response = [
            "success" => true,
        ];

        return $this->json($response);
    }

    /**
     * @Route("/suppression-multiple-definitive", name="delete_definitive_multiple")
     */
    public function deleteDefinitiveMultiple(Request $request): Response
    {
        $data = $request->request->all();
        if (!$this->getUser()) {
            return $this->redirectToRoute("app_login");
        }

        $user = $this->getUser();

        $id_messages_to_archived = $data["id_message_to_archived"];
        // $route = $data["route"];

        foreach ($id_messages_to_archived as $id_message_to_archived)
        {
            $message = $this->courrierRepository->find($id_message_to_archived);
            if ($user->getEmail() != $message->getSender()->getEmail() ) {
                $message->setIsDeletedTrashRecipient(true);
            }

            if ($user->getEmail() == $message->getSender()->getEmail()) {
                $message->setIsDeletedTrashSender(true);
            }
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($message);
        $entityManager->flush();

        $response = [
            "success" => true,
        ];

        return $this->json($response);
    }

    /**
     * @Route("/restauration-multiple-definitive", name="restore_definitive_multiple")
     */
    public function restoreDefinitiveMultiple(Request $request): Response
    {
        $data = $request->request->all();

        if (!$this->getUser()) {
            return $this->redirectToRoute("app_login");
        }

        $user = $this->getUser();

        $id_messages_to_archived = $data["id_message_to_archived"];

        foreach ($id_messages_to_archived as $id_message_to_archived)
        {
            $message = $this->courrierRepository->find($id_message_to_archived);
            if ($user->getEmail() != $message->getSender()->getEmail() ) {
                $message->setIsDeletedRecipient(false);
            }

            if ($user->getEmail() == $message->getSender()->getEmail()) {
                $message->setIsDeletedSender(false);
            }
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($message);
        $entityManager->flush();

        $response = [
            "success" => true,
        ];

        return $this->json($response);
    }

    /**
     * @Route("/suppression-multiple", name="delete_multiple")
     */
    public function suppressionMultiple(Request $request): Response
    {
        $messages_checked = $request->request->get("checkbox_multiple");
        $entityManager = $this->getDoctrine()->getManager();

        if ($messages_checked == NULL){
            return $this->redirectToRoute('app_trash');
        }

        foreach ($messages_checked as $message_checked) {
            $message_id = (int)$message_checked;
            $message = $this->courrierRepository->find($message_id);
            $entityManager->remove($message);
        }

        $entityManager->flush();

        return $this->redirectToRoute('app_trash');
    }

    /**
     * Supprime definitive le message dans le corbeille
     * @Route("/{id}/suppression", name="delete_message")
     */
    public function suppression(Request $request, Courrier $message): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        if ($message->getSender()->getEmail() == $this->getUser()->getEmail()){
            $message->setIsDeletedTrashSender(true);
        } else{
            $message->setIsDeletedTrashRecipient(true);
        }

        $entityManager->persist($message);
        $entityManager->flush();

        return $this->redirectToRoute('app_trash');
    }

    /**
     * Restaure le message supprimé
     * @Route("/{id}/{route}/restaurer", name="restore_trash", methods={"GET"})
     */
    public function restaurer(Courrier $courrier,$route): Response
    {
        $user = $this->getUser();
        $entityManager = $this->getDoctrine()->getManager();

        // si l'utilisateur est l'emetteur
        if ($this->courrierRepository->findOneBy(['sender' => $user])){
            $courrier->setIsDeletedSender(false);
        }

        // si l'utilisateur est le recepteur
         if($this->courrierRepository->getCourrierReceivedBy($user)){
            $courrier->setIsDeletedRecipient(false);
         }

        $entityManager->persist($courrier);
        $entityManager->flush();

        return $courrier->getSender()->getEmail() == $user->getEMail() ? $this->redirectToRoute("app_outbox") : $this->redirectToRoute("app_mailbox");
    }
}
