<?php

namespace App\Controller;

use App\Entity\Courrier;
use App\Entity\SortingMessage;
use App\Form\CourrierType;
use App\Repository\CourrierRepository;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Dompdf\Dompdf;
use Dompdf\Options;

class ReadController extends AbstractController
{
    /**
     * Message lu
     * @Route("read/{id}/{route}/", name="read", methods={"GET"})
     */
    public function read(Courrier $courrier,CourrierRepository $courrierRepository,Request $request,$route,UserRepository $userRepository): Response
    {
        if (!$this->getUser()){
            return $this->redirectToRoute("app_login");
        }

        if ($route == "app_mailbox") {
            $courrier->setIsRead(true);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($courrier);
            $entityManager->flush();
        }
        $form = $this->createForm(CourrierType::class, $courrier);
        $formMsgTransfert = $this->createForm(CourrierType::class);
        $form->handleRequest($request);

        $email_user = $this->getUser()->getEmail();
        $sorting_message = new SortingMessage();
        $last_message_received = $courrierRepository->findLastMessageReceived($email_user);
        $recipient = $courrierRepository->findAllMessageReceivedBy($email_user,$sorting_message);

        return $this->render('mail/lu.html.twig', [
            'courrier' => $courrier,
            'last_messages' => $last_message_received,
            'form' => $form->createView(),
            'formMsgTransfert' => $formMsgTransfert->createView(),
            'route' => $route,
            'users' => $userRepository->findAll()
        ]);
    }

    /**
     * @Route("/print/{id}", name="app_print")
     */
    public function usersDataDownload(Courrier $courrier)
    {
        //dd($courrier);
        // On définit les options du PDF
        $pdfOptions = new Options();
        // Police par défaut
        $pdfOptions->set('defaultFont', 'Arial');
        $pdfOptions->setIsRemoteEnabled(true);

        // On instancie Dompdf
        $dompdf = new Dompdf($pdfOptions);
        $context = stream_context_create([
            'ssl' => [
                'verify_peer' => FALSE,
                'verify_peer_name' => FALSE,
                'allow_self_signed' => TRUE
            ]
        ]);
        $dompdf->setHttpContext($context);

        // On génère le html
        $html = $this->renderView('pdf_template/pdf.html.twig',[
            'courier' => $courrier
        ]);

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();

        // On génère un nom de fichier
        $fichier = 'fichier-'. $courrier->getId() .'.pdf';

        // On envoie le PDF au navigateur
        $dompdf->stream($fichier, [
            'Attachment' => true
        ]);

        return new Response();
    }

}
