<?php

namespace App\Controller;


use App\Repository\CourrierRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TchatController extends AbstractController
{
    // Discussion instané ou Tchat
    /**
     * @Route("/tchat", name="app_tchat")
     */
    public function index(CourrierRepository $courrierRepository): Response
    {
        $recipient = $this->getUser();

        $email_received = $recipient->getEmail();

        $last_message_received = $courrierRepository->findLastMessageReceived($email_received);

        return $this->render('tchat/tchat.html.twig', [
            'controller_name' => 'TrashController',
            'last_messages' => $last_message_received,
        ]);
    }
}
