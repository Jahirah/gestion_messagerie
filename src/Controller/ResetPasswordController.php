<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\NewPasswordForgotPasswordType;
use App\Form\ResetPasswordRequestFormType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;


/**
 * @Route("/mot-de-passe-oublie")
 */
class ResetPasswordController extends AbstractController
{
    // Réinitialisation de mot de passe
    private $tokenGenerator;
    private $em;
    private $hasher;
    private $userRepository;
    private $mailer;

    public function __construct(TokenGeneratorInterface $tokenGenerator, EntityManagerInterface $em,
                                UserPasswordHasherInterface $hasher, UserRepository $userRepository, MailerInterface $mailer)
    {
        $this->tokenGenerator = $tokenGenerator;
        $this->em = $em;
        $this->hasher = $hasher;
        $this->userRepository = $userRepository;
        $this->mailer = $mailer;
    }

    /**
     * Affiche form de réinitialisation
     *
     * @Route("/", name="app_reset")
     */
    public function request(Request $request): Response
    {
        $form = $this->createForm(ResetPasswordRequestFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            return $this->processSendingPasswordResetEmail(
                $form->get('email')->getData()
            );
        }

        return $this->render('reset_password/request.html.twig', [
            'requestForm' => $form->createView(),
        ]);
    }

    /**
     * Fournir url pour réinitialiser le mot de passe
     * @Route("/reinitialiser/{token}", name="app_reset_password")
     * @param Request $request
     * @param UserPasswordHasherInterface $userPasswordHasherInterface
     * @param string $token
     * @return Response
     */
    public function reset(Request $request, UserPasswordHasherInterface $userPasswordHasherInterface, string $token): Response
    {
        $path_reset_password = $this->generateUrl('app_reset_password', ['token' => $token]);

        $form_reset_password = $this->createForm(NewPasswordForgotPasswordType::class,null,[
            'action' => $path_reset_password,
            'method' => 'POST'
        ]);

        $form_reset_password->handleRequest($request);

        if ($form_reset_password->isSubmitted() && $form_reset_password->isValid()){
            $new_password = $form_reset_password->get("new_password_forgot_password")->getData();

            if (!empty($token)){
                $user = $this->userRepository->findOneBy(['resetToken' => $token]);
                $user->setResetToken(NULL);
                $new_password = $this->hasher->hashPassword($user, $new_password);
                $user->setPassword($new_password);

                $this->em->persist($user);
                $this->em->flush();

                return $this->redirectToRoute('app_login');
            }
        }

        return $this->render('reset_password/reset.html.twig', [
            'resetForm' => $form_reset_password->createView(),
        ]);
    }

    private function processSendingPasswordResetEmail(string $emailFormData): RedirectResponse
    {
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'email' => $emailFormData,
        ]);

        if (empty($user)){
            return $this->redirectToRoute("app_login");
        }

        $token = $this->tokenGenerator->generateToken();

        if (empty($token)){
            return $this->redirectToRoute("app_login");
        }

        $user->setResetToken($token);
        $this->em->persist($user);
        $this->em->flush();

        $url_reset_password = $this->generateUrl('app_reset_password',['token' => $token], UrlGeneratorInterface::ABSOLUTE_URL);

        $fromUser = new Address('lewis@gmail.com', 'security');
        $toUser = $user->getEmail();
        $object = "Réinitialisation de mot de passe";

        $email = (new TemplatedEmail())
            ->from($fromUser)
            ->to($toUser)
            ->subject($object)
            ->htmlTemplate('reset_password/email.html.twig')
            ->context(compact('fromUser', 'toUser', 'object','url_reset_password'))
        ;

        $this->mailer->send($email);

        return $this->redirectToRoute('msg_reset');
    }
}
