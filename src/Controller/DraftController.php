<?php

namespace App\Controller;

use App\Entity\Courrier;
use App\Entity\Files;
use App\Form\CourrierType;
use App\Repository\CourrierRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DraftController extends AbstractController
{
    // Brouillon

    /**
     * @Route("/draft", name="app_draft")
     */
    public function list(CourrierRepository $courrierRepository)
    {
        $recipient = $this->getUser();

        $email_received = $recipient->getEmail();

        $last_message_received = $courrierRepository->findLastMessageReceived($email_received);

        //modal pour nouveau message
        $form = $this->createForm(CourrierType::class)->createView();

        return $this->render('mail/brouillon.html.twig', [
            'drafts' => $courrierRepository->DraftRepository($this->getUser()),
            'last_messages' => $last_message_received,
            'form' => $form
        ]);
    }

    /**
     * @Route("/draft/new", name="new_draft", methods={"GET","POST"})
     */
    public function draft(Request $request,UserRepository $userRepository): Response
    {
        //dd($request->files->all());
        $fileReauest = $request->files->all();
        //dd($fileReauest['courrier']['file']);
        $files = $fileReauest['courrier']['file'] ? $fileReauest['courrier']['file'] : null;

        $draft = new Courrier();
        $form = $this->createForm(CourrierType::class, $draft);
        $form->handleRequest($request);
        $recipients = $request->request->get('courrier')['recipient'];
        $sender = $this->getUser();
        $draft->setIsDraft(true);
        $draft->setSender($sender);
        $draft->setMessage($_POST['message_draft']);
        $draft->setObject($request->request->get('courrier')['object']);

        if ($files != null){
            foreach ($files as $file){
                $file_renamed = uniqid().".".$file->guessExtension();
                $file->move($this->getParameter('attachment_directory'), $file_renamed);

                $file = new Files();
                $file->setLibelle($file_renamed);
                $draft->addFile($file);
            }
        }

        foreach ($recipients as $recipient){
            $recipient_id = (int) $recipient;
            $recipient = $userRepository->find($recipient_id);
            $draft->addRecipient($recipient);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($draft);
        $entityManager->flush();

        return $this->json(1);
    }

    /**
     * @Route("/{id}/send-draft", name="send_draft", methods={"GET","POST"})
     */
    public function edit(Request $request, Courrier $courrier,CourrierRepository $courrierRepository,UserRepository $userRepository): Response
    {
        $formDraft = $this->createForm(CourrierType::class, $courrier);
        $formDraft->handleRequest($request);

        $recipient = $this->getUser();
        $email_received = $recipient->getEmail();

        $last_message_received = $courrierRepository->findLastMessageReceived($email_received);
        if ($request->isXmlHttpRequest()) {
            //dd($request->files->all());

            $object = $_POST['message_object'];
            $message = $_POST['message_draft'];
            $files = $request->files->all()['file_draft'] ? $request->files->all()['file_draft'] : null;
            foreach ($courrier->getRecipient() as $recipient){
                if($courrier->getRecipient() ){
                    $courrier->addRecipient($recipient);
                }
            }

            $courrier->setIsDraft(false);
            $courrier->setCreatedAt(new \DateTimeImmutable());
            $courrier->setMessage($message);
            $courrier->setObject($object);

            if ($files != null){
                foreach ($files as $file){
                    $file_renamed = uniqid().".".$file->guessExtension();
                    $file->move($this->getParameter('attachment_directory'), $file_renamed);

                    $file = new Files();
                    $file->setLibelle($file_renamed);
                    $courrier->addFile($file);
                }
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->json(1);
        }

        return $this->render('mail/brouillon.edit.html.twig', [
            'formDraft' => $formDraft->createView(),
            'last_messages' => $last_message_received,
            'form' => $this->createForm(CourrierType::class)->createView(),
            'draft' => $courrier,
            'users' => $userRepository->findAll(),
            'courrier' => $courrier
        ]);
    }
}

