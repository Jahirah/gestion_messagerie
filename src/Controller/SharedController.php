<?php

namespace App\Controller;

use App\Entity\Courrier;
use App\Entity\Files;
use App\Form\PartagerType;
use App\Repository\CourrierRepository;
use App\Repository\FilesRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SharedController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    // Message partagé
    /**
     * @Route("/shared", name="app_shared")
     */
    public function index(CourrierRepository $courrierRepository): Response
    {
        $recipient = $this->getUser();

        $email_received = $recipient->getEmail();

        $last_message_received = $courrierRepository->findLastMessageReceived($email_received);
        
        return $this->render('mail/partager.html.twig', [
            'controller_name' => 'SharedController',
            'last_message' => $last_message_received,
        ]);
    }

    /**
     * @Route("partager/{id}/message", name="share_message")
     */
    public function shareMessage(Request $request, Courrier $courrier, $id,UserRepository $userRepository,FilesRepository $filesRepository)
    {
        if ($this->getUser()){
            $this->redirectToRoute("app_login");
        }

        $message = new Courrier();
        $sender = $this->getUser();
        $date_created = new \DateTimeImmutable();

        $object = $_POST['message_object'];
        $msg = $_POST['message_share'];
        $recipients = isset($_POST['user']) ? $_POST['user'] : null;
        $files = $courrier->getFiles();
        $filesUpload = $request->files->all()['file_share'] ? $request->files->all()['file_share'] : null;

<<<<<<< HEAD
            foreach ($recipients as $recipient){
=======
        $userTab = [];

        if ($_user_cc_to){
            foreach ($_user_cc_to as $cc){
                $userTab [] = $cc;
            }
        }
        if($recipients){
            foreach ($recipients as $recipient){
                $userTab [] = $recipient;
            }
        }
        if($_user_cci_to){
            foreach ($_user_cci_to as $cci){
                $userTab [] = $cci;
            }
        }

        if (isset($_user_cc_to)){
            $message_cc_to = $_user_cc_to;
            $email_cc = [];
            //dd($message_cc_to);
            for ($i = 0; $i < count($message_cc_to); $i++)
            {
                $user = $userRepository->find($message_cc_to[$i]);
                array_push($email_cc,$user->getEmail());
            }

            $message->setMessageCc($email_cc);
        }

        if (isset($_user_cci_to)){
            $message_cci_to = $_user_cci_to;
            $email_cci = [];
            //dd($message_cc_to);
            for ($i = 0; $i < count($message_cci_to); $i++)
            {
                $user = $userRepository->find($message_cci_to[$i]);
                array_push($email_cci,$user->getEmail());
            }

            $message->setMessageCci($email_cci);
        }

            foreach ($userTab as $recipient){
>>>>>>> e6ed973ef29c1e46740bee60205d3d320d3fef1b
                $recipient_id = (int) $recipient;
                $recipient = $userRepository->find($recipient_id);
                $message->addRecipient($recipient);
                $message->setObject($object);
                $message->setMessage($msg);
                $message->setSender($sender);
                $message->setFile($courrier->getFile());
                $message->setCreatedAt($date_created);
                $message->setIsShared(true);
            }

        foreach ($files as $file){
            $fileEntity = new Files();
            $fileEntity->setLibelle($file->getLibelle());
            $message->addFile($fileEntity);
        }

        if ($filesUpload != null){
            foreach ($filesUpload as $file){
                $file_renamed = uniqid().".".$file->guessExtension();
                $file->move($this->getParameter('attachment_directory'), $file_renamed);

                $file = new Files();
                $file->setLibelle($file_renamed);
                $message->addFile($file);
            }
        }

            $this->em->persist($message);
            $this->em->flush();

            return $this->json(1);
    }
}
