<?php

namespace App\Controller;

use App\Form\CourrierType;
use App\Entity\SortingMessage;
use App\Repository\CourrierRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AllMessagesController extends AbstractController
{
    /**
     * Affiche tous les messages
     * @Route("/messages", name="all_messages")
     * @param CourrierRepository $courrierRepository
     * @return Response
     */
    public function allMessages(CourrierRepository $courrierRepository): Response
    {
        if (!$this->getUser()) {
            return $this->redirectToRoute('app_login');
        }

        $email_user = $this->getUser()->getEmail();
        $sorting_message = new SortingMessage();
        $last_messages = $courrierRepository->findLastMessageReceived($email_user);

        $recipient = $courrierRepository->findAllMessageReceivedBy($email_user, $sorting_message);

        $outBox = $courrierRepository->OutBoxRepository($this->getUser());

        $messages = array_merge($recipient, $outBox);

        //modal pour nouveau message
        $form = $this->createForm(CourrierType::class)->createView();

        return $this->render('mail/messages.html.twig', compact("messages","last_messages","form"));
    }
}

