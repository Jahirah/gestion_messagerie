<?php

namespace App\Controller;

use App\Entity\Courrier;
use App\Entity\User;
use App\Entity\Files;
use App\Form\CourrierType;
use App\Repository\UserRepository;
use App\Entity\SortingMessage;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use App\Form\SortingMessageType;
use App\Repository\CourrierRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
* @Route("/messagerie")
*/
class MessagerieController extends AbstractController{
    /**
    * @Route("/boite-de-reception",name="messagerie_boite_reception")
    */	
	public function boiteReception(Request $request,CourrierRepository $messageRepository):Response{
		
        $recipient = $this->getUser();

        if (empty($recipient)){
            return $this->redirectToRoute('app_login');
        }

        $sorting_message = new SortingMessage();
        $search_form = $this->createForm(SortingMessageType::class, $sorting_message);
        $search_form->handleRequest($request);
        $form = $this->createForm(CourrierType::class);

        $email_received = $recipient->getEmail();

        // message non reçu
        $email_receiveds = $messageRepository->findAllMessageReceivedBy($email_received, $sorting_message);

        // notification
        $last_messages = $messageRepository->findLastMessageReceived($email_received);

        // compte message reçu non lu
        $numberMsgNotRead = $messageRepository->countMessageReceivedNotReaded($email_received)[0]['nbre_isNotRead'];

        return $this->render('messagerie/reception.html.twig', [
            'email_receiveds' => $email_receiveds,
            'numberMsgNotRead' => $numberMsgNotRead,
            'last_messages' => $last_messages,
            'search_form' => $search_form->createView(),
            'form'=>$form->createView()
        ]);
	}
	
    /*----------------Nouveau message------------*/
	
    /**
     * @Route("/new", name="messagerie_new")
    */
    //public function new(Request $request, UserRepository $UserRepository,CourrierRepository $courrierRepository, MailerInterface $mailer): Response{	
    public function new(Request $request, MailerInterface $mailer,UserRepository $UserRepository,CourrierRepository $courrierRepository): Response{
        if (!$this->getUser()){
            return $this->redirectToRoute("app_login");
        }
		$submit=0;

		$user=$this->getUser();
        $emails_user_in_db = $UserRepository->findAll();
        $emails = [];

        for($i = 0; $i < count($emails_user_in_db); $i++){
            array_push($emails, $emails_user_in_db[$i]->getEmail());
        }

        $nouveau_message = new Courrier;
        $form = $this->createForm(CourrierType::class, $nouveau_message);
        
        $form->handleRequest($request);

        $lastEmail = $this->getUser();
        $email_received = $lastEmail->getEmail();
        $last_message_received = $courrierRepository->findLastMessageReceived($email_received);
        if($form->isSubmitted() && $form->isValid()){
			$submit=1;
            $recipients = $form->get("recipient")->getData();
            $files_uploaded = $form->get("file")->getData();
            $message_cc_to = $form->get('messageCcTo')->getData();
            $message_cci_to = $form->get('messageCciTo')->getData();
            if (count($files_uploaded) > 0){
                foreach ($files_uploaded as $file_uploaded){
                    $file_renamed = uniqid().".".$file_uploaded->guessExtension();
                    $file_uploaded->move($this->getParameter('attachment_directory'), $file_renamed);

                    $file = new Files();
                    $file->setLibelle($file_renamed);
                    $nouveau_message->addFile($file);
                }
            }

            foreach ($recipients as $recipient){
                $nouveau_message->addRecipient($recipient);
				/*----------codes rajoutés---------*/
				$mail = new Email();
				$expediteur=$user->getEMail();
				$destinataire=$recipient->getEmail();
				$object=$form->get("object")->getData();
				$message=$form->get("message")->getData();
				
				$object=($object)?$object:"";
				$message=($message)?$message:"";			
				
				
				$mail->from($expediteur);
				$mail->to($destinataire);
				$mail->Subject($object);
				$mail->html($message);
				$mailer->send($mail);
				
				/*-------fin----------*/
            }

            if (count($message_cc_to)){
                $message_cc_to = $message_cc_to->toArray();
                $email_cc = [];
                for ($i = 0; $i < count($message_cc_to); $i++){
                    $email_copie = $message_cc_to[$i]->getEmail();

                    if (!in_array($email_copie, $emails)){
                        $email_copie_exploded = explode("@",$email_copie);
                        $domain_name = end($domain_name);
                        if ($domain_name == "gmail.com"){
                            $email_to_send = (new TemplatedEmail())
                                ->from($this->getUser()->getEmail())
                                ->to("asonmanuchristian@gmail.com")
                                ->subject($form->get("object")->getData())
                                ->htmlTemplate("mail/content_email.html.twig")
                                ->context([]);
                            $mailer->send($email_to_send);
                        }
                    }

                    array_push($email_cc,$email_copie);
                }

                $nouveau_message->setMessageCc($email_cc);
            }

            if (count($message_cci_to)){
                $message_cci_to = $message_cci_to->toArray();
                $email_cci = [];

                for ($i = 0; $i < count($message_cci_to); $i++)
                {
                    array_push($email_cci,$message_cci_to[$i]->getEmail());
                }

                $nouveau_message->setMessageCci($email_cci);
            }
            
            $nouveau_message->setSender($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($nouveau_message);

            $em->flush();

            $this->addFlash("message", "Message envoyé avec succès.");
        }
        return $this->render('messagerie/new.html.twig',[
			'form'=>$form->createView(),
			'submit'=>$submit,
		]);
    }

    /**
     * @Route("/reply", name="app_reply")
     */
    public function sendMailAjax(Request $request,UserRepository $userRepository)
    {
        //dd($request->files->all()['file']);
        $fileRequest = $request->files->all()['file'];
        $files = $fileRequest ? $fileRequest : null;
        $courrier = new Courrier;
        $sender = $this->getUser();
        $courrier->setSender($sender);
        $courrier->setMessage($_POST['msg']);
        $courrier->setObject($request->request->get('subject'));

        if ($files != null){
            foreach ($files as $file){
                $file_renamed = uniqid().".".$file->guessExtension();
                $file->move($this->getParameter('attachment_directory'), $file_renamed);

                $file = new Files();
                $file->setLibelle($file_renamed);
                $courrier->addFile($file);
            }
        }

        $recipient_mail = $request->request->get('emailto');
        $recipient = $userRepository->findBy(['email' => $recipient_mail]);
        $courrier->addRecipient($recipient[0]);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($courrier);
        $entityManager->flush();

        return $this->json(1);
    }

    /**
     * @Route("/add-user", name="add_user_ajax")
     */
    public function addUserAjax(Request $request, UserPasswordHasherInterface $userPasswordHasherInterface)
    {
        $email_new_user = $request->query->get("email_new_user");
        $password_new_user = "password";
        $email_new_user_exploded = explode("@",$email_new_user);
        $username_new_user = $email_new_user_exploded[0];

        $user = new User();
        $user->setEmail((string) $email_new_user);
        $user->setUsername((string) $username_new_user);
        $user->setPassword($userPasswordHasherInterface->hashPassword($user, $password_new_user));
        $user->setRoles([]);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

        $id_new_user = $user->getId();

        $response = [
            "success" => true,
            "id_new_user" => $id_new_user
        ];

        return $this->json($response);
    }
	public function printer($array){
		
		echo "<pre>";
		print_r($array);
		echo "</pre>";
		
	}
}
