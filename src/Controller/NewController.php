<?php

namespace App\Controller;

use App\Entity\Courrier;
use App\Entity\User;
use App\Entity\Files;
use App\Form\CourrierType;
use App\Repository\UserRepository;
use App\Repository\CourrierRepository;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class NewController extends AbstractController
{
    // Nouveau message
    /**
     * @Route("/new", name="app_new")
     */
    public function index(Request $request, UserRepository $UserRepository,
                          CourrierRepository $courrierRepository, MailerInterface $mailer): Response
    {
        if (!$this->getUser()){
            return $this->redirectToRoute("app_login");
        }

        $emails_user_in_db = $UserRepository->findAll();
        $emails = [];

        for($i = 0; $i < count($emails_user_in_db); $i++){
            array_push($emails, $emails_user_in_db[$i]->getEmail());
        }

        $nouveau_message = new Courrier;
        $form = $this->createForm(CourrierType::class, $nouveau_message);
        
        $form->handleRequest($request);

        $lastEmail = $this->getUser();
        $email_received = $lastEmail->getEmail();
        $last_message_received = $courrierRepository->findLastMessageReceived($email_received);

//        if($form->isSubmitted() && $form->isValid()){
        if($form->isSubmitted() && $request->isMethod("POST")){
            $recipients = $form->get("recipient")->getData();

            //dd($request->request->get("courrier[messageCcTo][]"));
            //$message = $_POST['msg'];
            //dd($form->get("messageCcTo")->getData());

            $files_uploaded = $form->get("file")->getData();
            $message_cc_to = $form->get('messageCcTo')->getData();
            $message_cci_to = $form->get('messageCciTo')->getData();

            if (count($files_uploaded) > 0){
                foreach ($files_uploaded as $file_uploaded){
                    $file_renamed = uniqid().".".$file_uploaded->guessExtension();
                    $file_uploaded->move($this->getParameter('attachment_directory'), $file_renamed);

                    $file = new Files();
                    $file->setLibelle($file_renamed);
                    $nouveau_message->addFile($file);
                }
            }

            foreach ($recipients as $recipient){
                $nouveau_message->addRecipient($recipient);
            }

            if (count($message_cc_to)){
                $message_cc_to = $message_cc_to->toArray();
                $email_cc = [];

                for ($i = 0; $i < count($message_cc_to); $i++)
                {
                    $email_copie = $message_cc_to[$i]->getEmail();

                    if (!in_array($email_copie, $emails)){
                        $email_copie_exploded = explode("@",$email_copie);
                        $domain_name = end($domain_name);

                        if ($domain_name == "gmail.com"){
                            $email_to_send = (new TemplatedEmail())
                                ->from($this->getUser()->getEmail())
                                ->to("asonmanuchristian@gmail.com")
                                ->subject($form->get("object")->getData())
                                ->htmlTemplate("mail/content_email.html.twig")
                                ->context([]);

                            $mailer->send($email_to_send);
                        }
                    }

                    array_push($email_cc,$email_copie);
                }

                $nouveau_message->setMessageCc($email_cc);
            }

            if (count($message_cci_to)){
                $message_cci_to = $message_cci_to->toArray();
                $email_cci = [];

                for ($i = 0; $i < count($message_cci_to); $i++)
                {
                    array_push($email_cci,$message_cci_to[$i]->getEmail());
                }

                $nouveau_message->setMessageCci($email_cci);
            }
            
            $nouveau_message->setSender($this->getUser());
            //$nouveau_message->addRecipient($recipient);
            //$nouveau_message->setMessage($message);
            $em = $this->getDoctrine()->getManager();
            $em->persist($nouveau_message);

            $em->flush();

            $this->addFlash("message", "Message envoyé avec succès.");

            // return $this->json(1);
            return $this->redirectToRoute('app_outbox');
        }

        // return $this->json(1);
        return $this->redirectToRoute('app_outbox');
    }

    /**
     * @Route("/reply", name="app_reply")
     */
    public function sendMailAjax(Request $request,UserRepository $userRepository)
    {
        //dd($request->files->all()['file']);
        $fileRequest = $request->files->all()['file'];
        $files = $fileRequest ? $fileRequest : null;
        $courrier = new Courrier;
        $sender = $this->getUser();
        $courrier->setSender($sender);
        $courrier->setMessage($_POST['msg']);
        $courrier->setObject($request->request->get('subject'));

        if ($files != null){
            foreach ($files as $file){
                $file_renamed = uniqid().".".$file->guessExtension();
                $file->move($this->getParameter('attachment_directory'), $file_renamed);

                $file = new Files();
                $file->setLibelle($file_renamed);
                $courrier->addFile($file);
            }
        }

        $recipient_mail = $request->request->get('emailto');
        $recipient = $userRepository->findBy(['email' => $recipient_mail]);
        $courrier->addRecipient($recipient[0]);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($courrier);
        $entityManager->flush();

        return $this->json(1);
    }

    /**
     * @Route("/add-user", name="add_user_ajax")
     */
    public function addUserAjax(Request $request, UserPasswordHasherInterface $userPasswordHasherInterface)
    {
        $email_new_user = $request->query->get("email_new_user");
        $password_new_user = "password";
        $email_new_user_exploded = explode("@",$email_new_user);
        $username_new_user = $email_new_user_exploded[0];

        $user = new User();
        $user->setEmail((string) $email_new_user);
        $user->setUsername((string) $username_new_user);
        $user->setPassword($userPasswordHasherInterface->hashPassword($user, $password_new_user));
        $user->setRoles([]);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

        $id_new_user = $user->getId();

        $response = [
            "success" => true,
            "id_new_user" => $id_new_user
        ];

        return $this->json($response);
    }
}
