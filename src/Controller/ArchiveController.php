<?php

namespace App\Controller;

use App\Entity\Courrier;
use App\Form\CourrierType;
use App\Repository\CourrierRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ArchiveController extends AbstractController
{
    private $courrierRepository;

    // Messages archivés

    /**
     * ArchiveController constructor.
     * @param CourrierRepository $courrierRepository
     */
    public function __construct(CourrierRepository $courrierRepository)
    {
        $this->courrierRepository = $courrierRepository;
    }

    /**
     * @Route("/archive", name="app_archive")
     */
    public function index(CourrierRepository $courrierRepository): Response
    {
        $recipient = $this->getUser();

        $email_received = $recipient->getEmail();

        $last_message_received = $courrierRepository->findLastMessageReceived($email_received);
        $outBoxArchive = $courrierRepository->OutBoxArchive($this->getUser());
        $inboxArchive = $courrierRepository->findAllMessageArchivedBy($email_received);
        
        //modal pour nouveau message
        $form = $this->createForm(CourrierType::class)->createView();

        return $this->render('mail/archive.html.twig', [
            'archives' => array_merge($inboxArchive, $outBoxArchive),
            'last_messages' => $last_message_received,
            'form' => $form
        ]);
    }

    /**
     * @Route("/{id}/{route}/archive", name="archive", methods={"GET"})
     */
    public function archive(Courrier $courrier, $route): Response
    {
        if ($route == "app_outbox") {
            $courrier->setIsArchivedSender(true);
        }
        if ($route == "app_mailbox") {
            $courrier->setIsArchivedRecipient(true);
        }
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($courrier);
        $entityManager->flush();

        return $this->redirectToRoute('app_archive');
    }

    /**
     * @Route("/archive-multiple", name="archive_multiple")
     */
    public function archiveMultiple(Request $request): Response
    {
        $data = $request->request->all();

        if (!$this->getUser()) {
            return $this->redirectToRoute("app_login");
        }

        $user = $this->getUser();

        $id_messages_to_archived = $data["id_message_to_archived"];
        $route = $data["route"];
        $entityManager = $this->getDoctrine()->getManager();

        foreach ($id_messages_to_archived as $id_message_to_archived) {
            $message = $this->courrierRepository->find($id_message_to_archived);

            if ($route == "app_mailbox" || $user->getEmail() != $message->getSender()->getEmail() ) {
                $message->setIsArchivedRecipient(true);
            }

            if ($route == "app_outbox" || $user->getEmail() == $message->getSender()->getEmail()) {
                $message->setIsArchivedSender(true);
                $message->setIsArchivedRecipient(true);
            }
        }

        $entityManager->persist($message);
        $entityManager->flush();

        $response = [
            "success" => true,
        ];

        return $this->json($response);
    }

    /**
     * @Route("/desarchive-multiple", name="unarchive_multiple")
     */
    public function unArchiveMultiple(Request $request): Response
    {
        $data = $request->request->all();

        if (!$this->getUser()) {
            return $this->redirectToRoute("app_login");
        }

        $user = $this->getUser();

        $id_messages_to_archived = $data["id_message_to_archived"];
        $route = $data["route"];
        $entityManager = $this->getDoctrine()->getManager();

        foreach ($id_messages_to_archived as $id_message_to_archived) {
            $message = $this->courrierRepository->find($id_message_to_archived);

            if ($user->getEmail() == $message->getSender()->getEmail()) {
                $message->setIsArchivedSender(false);
                $message->setIsArchivedRecipient(false);
            }

            if (!($user->getEmail() == $message->getSender()->getEmail())) {
                $message->setIsArchivedRecipient(false);
            }
        }

        $entityManager->persist($message);
        $entityManager->flush();

        $response = [
            "success" => true,
        ];

        return $this->json($response);
    }

    /**
     * @Route("/{id}/unarchive", name="unarchive", methods={"GET"})
     */
    public function unArchive(Courrier $courrier)
    {
        $courrier->getSender() == $this->getUser() ? $courrier->setIsArchivedSender(false) and $courrier->setIsArchivedRecipient(false) : $courrier->setIsArchivedRecipient(false);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($courrier);
        $entityManager->flush();

        return $courrier->getSender()->getEmail() == $this->getUser()->getEMail() ? $this->redirectToRoute("app_outbox") : $this->redirectToRoute("app_mailbox");
    }
}
